<?php
if ($_GET['action'] === 'ajax') {
    if (!empty($_POST['submit']) && $_POST['submit'] == 'additional_pairings') {
        $to_check = $res_messages = $errors = [];
        $data = explode(',', $_POST['data']);

        if (is_array($data)) {
            foreach ($data as $name) {
                $to_check[] = ['name' => 'Pairing name', 'type' => 'text', 'value' => trim($name)];
            }
        }

        if (!empty($to_check)) {
            $errors = check_inputs($to_check);

            if (empty($errors)) {
                foreach ($data as $name) {
                    $errors = array_merge($errors, add_pairing(['pairing_name' => $name]));
                }

                if (empty($errors)) {
                    $res_messages = [['status' => 'success', 'message' => 'Pairing was successfully added.']];
                } else {
                    foreach ($errors as $error) {
                        $res_messages[] = ['status' => 'error', 'message' => $error];
                    }
                }
            } else {
                foreach ($errors as $error) {
                    $res_messages[] = ['status' => 'error', 'message' => $error];
                }
            }
        } else {
            $res_messages = [['status' => 'error', 'message' => 'You submitted an empty value. Nothing happened.']];
        }

        echo json_encode($res_messages);
    }

    die;
}
